---
layout: job_page
title: "Backend Director"
---

As the Backend Director, you report to the [VP of
Engineering](https://about.gitlab.com/jobs/vp-of-engineering). You will
lead all backend groups within engineering.

## Responsibilities

* Identify hiring needs and lead recruitment of backend engineers
* Work with product team, VP of Engineering, and leads to prioritize backend work
* Coordinate with all backend leads to deliver releases on schedule
* Direct resources to priority features and bug fixes when needed
* Deliver features that increase revenue and reduce costs of GitLab.com
* Work with support team to triage critical issues
* Monitor production-level issues and help resolve them
* Identify and propose solutions to improve pace of development
* Identify and fill knowledge gaps with new technologies relevant to company's future
* Train team members on proper coding and review techniques
* Provide coaching and feedback to direct reports
* Review merge requests made by developers
* Define best practices and coding standards for backend team
* Deliver input on engineering organizational changes in consultation with the CEO, CTO, and VP of Engineering
* Coordinates with frontend and UX leads to form teams for major features/moonshots

## Requirements

* Experience leading a team of software developers
* Experience developing large-scale applications with high-availability and performance requirements
* Able to manage multiple, concurrent projects
* Ability to reason about software, algorithms, and performance from a high level
* Passion for open source
* Experience working on a production-level Ruby application, preferably using Rails.
* Strong written communication skills
* Self-motivated and have strong organizational skills
* You share our [values](/handbook/#values), and work in accordance with those values.
* [A technical interview](/jobs/#technical-interview) is part of the hiring process for this position.

## Relevant links

- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)
