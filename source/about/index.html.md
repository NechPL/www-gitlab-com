---
layout: markdown_page
title: About Us
---

- TOC
{:toc}

## GitLab Inc.

GitLab Inc. is a company based on the [GitLab open-source project](https://gitlab.com/gitlab-org/gitlab-ce/).
GitLab is an integrated product that unifies issues, code review, CI and CD into a single UI.
GitLab Inc. offers [self hosted products](https://about.gitlab.com/products/) and [SaaS plans for GitLab.com](https://about.gitlab.com/gitlab-com/).
GitLab is an open source project project with a large community, over 1700 people worldwide have [contributed to GitLab]!
GitLab Inc. has an is an active participant in this community, see [our stewardship of GitLab CE](#stewardship) for more information.
For a more customer focused overview see our [pitches](https://about.gitlab.com/handbook/sales/#pitches).
We have a lot of information available online, see the [primer](https://about.gitlab.com/primer/) for a good start.

## A brief history of GitLab

Please see [the history page](https://about.gitlab.com/history/).

## Vision

At GitLab we have one vision.
**Everyone can contribute** to all digital content.
For more information see the our [strategy](https://about.gitlab.com/strategy/).

## Logo

Our <a href="https://en.wikipedia.org/wiki/Japanese_raccoon_dog">Tanuki</a> logo
symbolizes this with a smart animal that works in a group to achieve a common goal.
Please see our [press page](https://about.gitlab.com/press/) to download the logo.

## Values

Please see the [values section in our handbook](https://about.gitlab.com/handbook/values).

## Our stewardship of GitLab CE<a name="stewardship"></a><a name="our-stewardship-of-gitlab-ce"></a>

Please see the [the stewardship page](https://about.gitlab.com/stewardship/).

## Handbook

If you're interested, most of our internal procedures can be found in <a href="/handbook">publicly viewable handbooks</a>.

## Donations<a name="donations"></a>

Some people contact us because they would like to donate to GitLab.
If you have time to give please help spread the word about GitLab by mentioning us and/or <a href="https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md">contribute by creating and reviewing issues and merge requests</a>.
If you would like to give money please <a href="http://railsgirlssummerofcode.org/campaign/">donate to Rails Girls Summer of Code</a> in our name.

## Location
GitLab is an open source project with [more than 1500 people](http://contributors.gitlab.com/) contributing from all over the world.
GitLab Inc. has people in [more than 35 countries](https://about.gitlab.com/team/).
We're proud to be [remote only](http://www.remoteonly.org/).
You can taste a bit of the GitLab team culture by visiting
our <a href="https://about.gitlab.com/culture/">culture page</a>.

[This commit]: https://gitlab.com/gitlab-org/gitlab-ce/commit/0f43e98ef8c2da8908b1107f75b67cda2572c2c4
[first version of GitLab CI]: https://gitlab.com/gitlab-org/gitlab-ci/commit/52cd500ee64a4a82b9ff6752ee85028cd419fcbe
[GitLab Enterprise Edition]: https://about.gitlab.com/2013/08/22/introducing-gitlab-6-0-enterprise-edition/
[participate in Y Combinator]: https://about.gitlab.com/2015/03/04/gitlab-is-part-of-the-y-combinator-family/
[contributed to GitLab]: http://contributors.gitlab.com/
